<?php
namespace common\action;

use herosphp\core\Controller;

/**
 * common action, 这里一般把通用的代码写在这边，然后其他的action直接继承 CommonAction
 * @since           2015-01-28
 * @author          yangjian<yangjian102621@163.com>
 */
abstract class CommonAction extends Controller {}
